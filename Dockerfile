# https://hub.docker.com/_/alpine/
FROM alpine:3.11

MAINTAINER Rafal Masiarek <rafal@masiarek.pl>


RUN apk update \
 && apk upgrade \
 && apk add --no-cache \
            git \
            rsync \
            openssh-client \
            ca-certificates \
 && update-ca-certificates \
 && rm -rf /var/cache/apk/*
