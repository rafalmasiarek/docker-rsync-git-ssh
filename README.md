Rsync + Git + SSH Docker Image
========================

[![shield gitlabci badge](https://img.shields.io/gitlab/pipeline/rafalmasiarek/rsync-git-ssh-docker-image)](https://gitlab.com/rafalmasiarek/rsync-git-ssh-docker-image/-/pipelines/latest) [![shield docker badge](https://img.shields.io/docker/pulls/rafalmasiarek/rsync-git-ssh.svg)](https://hub.docker.com/r/rafalmasiarek/rsync-git-ssh)

### Mix of these three packages creates the perfect and universal collection of tools for creating automated CI / CD deployers.

## What is Rsync, SSH and Git?

SSH (Secure Shell) is a cryptographic network protocol for operating network services securely over an unsecured network. The best known example application is for remote login to computer systems by users.

Rsync is a utility for efficiently transferring and synchronizing files across computer systems, by checking the timestamp and size of files. It is commonly found on Unix-like systems and functions as both a file synchronization and file transfer program. The rsync algorithm is a type of
delta encoding, and is used for minimizing network usage. Zlib may be used for additional compression, and SSH or stunnel can be used for data security.

Git is a version control system for tracking changes in computer files and coordinating work on those files among multiple people. It is primarily used for source code management in software development, but it can be used to keep track of changes in any set of files. As a distributed revision control system it is aimed at speed, data integrity, and support for distributed, non-linear workflows.
